% print a matrix with brackets and optionnaly its name
function printMatrix(mat, name_mat = "")
    
    % get the dimensions
    [nb_lin, nb_col] = size(mat);
    
    % calculation of the indentation
    indent = "";
    if (length(name_mat) != 0)
        name_mat = cstrcat(name_mat, " = ");
        for i = [1:length(name_mat)]
            indent = cstrcat(indent, " ");
        endfor
    endif
    
    % calculation of the line which will has the name
    name_lin = ceil(nb_lin * 0.5);
    
    % vector which contains each column width (in nb of char)
    col_width = zeros(nb_col, 1);
    for i = [1:nb_lin]
        for j = [1:nb_col]
            tmp_width = length(num2str(mat(i, j)));
            if (tmp_width > col_width(j))
                col_width(j) = tmp_width;
            endif
        endfor
    endfor
    
    % matrix which contains nb of spaces to add to each coefficient
    mat_espaces_supp = zeros(nb_lin, nb_col);
    for i = [1:nb_lin]
        for j = [1:nb_col]
            mat_espaces_supp(i, j) = col_width(j) - length(num2str(mat(i, j)));
        endfor
    endfor
    
    % print the matrix
    for i = [1:nb_lin]
        if (i == name_lin)
            printf(name_mat);
        else
            printf(indent);
        endif
        if (i == 1)
            if (i == nb_lin)
                printf("[ %s ]\n\n", printLine(mat, mat_espaces_supp, i));
            else
                printf("⌈ %s ⌉\n", printLine(mat, mat_espaces_supp, i));
            endif
        elseif (i == nb_lin)
            printf("⌊ %s ⌋\n\n", printLine(mat, mat_espaces_supp, i));
        else
            printf("| %s |\n", printLine(mat, mat_espaces_supp, i));
        endif
    endfor
    
endfunction

% return a string of the line number i of mat and ajust spaces
function s = printLine(mat, mat_espaces_supp, i)
    
    between_coeff = "  ";
    s = "";
    nb_col = size(mat)(2);
    for j = [1:nb_col]
        s = cstrcat(s, num2str(mat(i, j)));
        for k = [1:mat_espaces_supp(i, j)]
            s = cstrcat(s, " ");
        endfor
        if (j != nb_col)
            s = cstrcat(s, between_coeff);
        endif
    endfor

endfunction
