# Octave functions and Octave code in LaTeX

## Octave functions

To use a function you can simply put the file which has the same name in your working directory.

#### `printMatriX(mat, name_mat = "")`

This function prints the matrix `mat` and imitates square brackets delimiters. The argument `name_mat` is optionnal.
For example :
```
A = [1, 22, 3, 4; 5, 666, 7, 8; 9, 10, 11, 12];
printMatrix(A);
```
will give you in the terminal
```
⌈ 1  22   3   4  ⌉
| 5  666  7   8  |
⌊ 9  10   11  12 ⌋

```
and for the same matrix `A`
```
printMatrix(A, "A");
```
will give you in the terminal
```
    ⌈ 1  22   3   4  ⌉
A = | 5  666  7   8  |
    ⌊ 9  10   11  12 ⌋

```

#### `mat2latex(mat, file_name = "stdout", env = "pmatrix", before_coeff = "", after_coeff = "")`

This function prints the matrix `mat` in LaTeX code in a text file (in the terminal by default).
You can copy and paste the code in your LaTeX source file or use `\input` if you have saved the matrix in a text file.

For example :
```
A = [1, 22, 3, 4; 5, 666, 7, 8; 9, 10, 11, 12];
mat2latex(A)
```
will give you in the terminal
```
\begin{pmatrix}
  1 & 22  & 3  & 4 \\
  5 & 666 & 7  & 8 \\
  9 & 10  & 11 & 12\\
\end{pmatrix}
```
and for the same matrix `A`
```
mat2latex(A, "myfile.tex", "bmatrix", "\\num{", "}");
```
will give you in the file `myfile.tex`
```
\begin{bmatrix}
  \num{1} & \num{22}  & \num{3}  & \num{4} \\
  \num{5} & \num{666} & \num{7}  & \num{8} \\
  \num{9} & \num{10}  & \num{11} & \num{12}\\
\end{bmatrix}
```
note that operation erase the old content of the file `myfile.tex` if this file already exists.

## Octave code in a LaTeX document

Check out the folder `octave-in-latex-doc-fr` to see a french LaTeX document with insertion of Octave code. The coloration of the Octave code was made for looking like the default syntax coloration of the GNU Octave editor.
It also emphasizes how the `mat2latex` function, described above, can be useful.
