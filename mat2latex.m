% write a matrix in LaTeX code in a text file (in the terminal by default)
function mat2latex(mat, file_name = "stdout", env = "pmatrix",
    before_coeff = "", after_coeff = "")

    default_file_name = "stdout";
    indent = 2;
    [nb_lin, nb_col] = size(mat);
    
    % determine if a file name was given
    file_name_given = false;
    if (length(file_name) != length(default_file_name))
        file_name_given = true;
    else
        for i = [1:length(file_name)]
            if (file_name(i) != default_file_name(i))
                file_name_given = true;
                break;
            endif
        endfor
    endif
    
    % vector which contains each column width (in nb of char)
    col_width = zeros(nb_col, 1);
    for i = [1:nb_lin]
        for j = [1:nb_col]
            tmp_width = length(num2str(mat(i, j)));
            if (tmp_width > col_width(j))
                col_width(j) = tmp_width;
            endif
        endfor
    endfor
    
    % matrix which contains nb of spaces to add to each coefficient
    mat_espaces_supp = zeros(nb_lin, nb_col);
    for i = [1:nb_lin]
        for j = [1:nb_col]
            mat_espaces_supp(i, j) = col_width(j) - length(num2str(mat(i, j)));
        endfor
    endfor

    % open file
    if (file_name_given)
        fid = fopen(file_name, "w");
    else
        fid = stdout;
    endif

    % print the matrix
    fprintf(fid, "\\begin{%s}\n", env);
    for i = [1:nb_lin]
        % indent
        for j = [1:indent]
            fprintf(fid, " ");
        endfor
        % i-th line
        for j = [1:nb_col]
            fprintf(fid, "%s%s%s", before_coeff, num2str(mat(i, j)),
                after_coeff);
            for k = [1:mat_espaces_supp(i, j)]
                fprintf(fid, " ");
            endfor
            if (j == nb_col)
                fprintf(fid, "\\\\\n");
            else
                fprintf(fid, " & ");
            endif
        endfor
    endfor
    fprintf(fid, "\\end{%s}\n", env);

    % close file
    if (file_name_given)
        fclose(fid);
    endif

endfunction