% renvoie la norme 2 du vecteur x
function y = norm2(x)
    y = sqrt(sum(x .* x));
endfunction